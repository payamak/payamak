# Payamak

> PHP library for sending SMS messages.

[![Packagist Version](https://img.shields.io/packagist/v/payamak/payamak.svg)][1]
[![PHP from Packagist](https://img.shields.io/packagist/php-v/payamak/payamak.svg)][2]
[![Travis (.com) branch](https://img.shields.io/gitlab/pipeline/payamak/payamak/master)][3]
[![Codecov](https://img.shields.io/codecov/c/gl/payamak/payamak.svg)][4]
[![Packagist](https://img.shields.io/packagist/l/payamak/payamak.svg)][5]
[![Twitter: nekofar](https://img.shields.io/twitter/follow/nekofar.svg?style=flat)][6]

[1]: https://packagist.org/packages/payamak/payamak
[2]: https://www.php.net/releases/7_2_0.php
[3]: https://gitlab.com/payamak/payamak
[4]: https://codecov.io/gl/payamak/payamak
[5]: https://gitlab.com/payamak/payamak/blob/master/LICENSE
[6]: https://twitter.com/nekofar

[//]: https://github.com/thephpleague/payamak-common
[//]: https://github.com/veronalabs/wp-sms

