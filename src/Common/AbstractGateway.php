<?php

/**
 * @package Payamak\Common
 * @subpackage AbstractGateway
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common;

use Http\Client\Common\HttpMethodsClient;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;
use Payamak\Common\Message\AbstractRequest;
use Payamak\Common\Message\RequestInterface;

/**
 * Class AbstractGateway
 *
 * @package Payamak\Common
 */
abstract class AbstractGateway implements GatewayInterface
{
    use ParametersTrait;

    /**
     * A HTTP client
     *
     * @var HttpMethodsClient
     */
    protected $httpClient;

    /**
     * Create a new gateway instance.
     *
     * @param HttpMethodsClient|null $httpClient A HTTP client to make API calls with.
     */
    public function __construct(?HttpMethodsClient $httpClient)
    {
        $this->httpClient = $httpClient ?: new HttpMethodsClient(
            HttpClientDiscovery::find(),
            MessageFactoryDiscovery::find()
        );

        $this->initialize();
    }

    /**
     * @inheritDoc
     * @return string
     */
    public function getShortName(): string
    {
        $className = get_called_class();

        if (0 === strpos($className, '\\')) {
            $className = substr($className, 1);
        }

        if (0 === strpos($className, 'Payamak\\')) {
            return trim(str_replace('\\', '_', substr($className, 8, -7)), '_');
        }

        return '\\' . $className;
    }

    /**
     * @inheritDoc
     * @return array
     */
    public function getDefaultParameters(): array
    {
        return [];
    }

    /**
     * Initialize the array with parameters.
     *
     * If any unknown parameters passed, they will be ignored.
     *
     * @param array $parameters An associative array of parameters.
     * @return $this.
     */
    public function initialize(array $parameters = [])
    {
        $this->parameters = [];

        // Combine default parameters with given.
        $parameters = array_merge(
            $this->getDefaultParameters(),
            $parameters
        );

        foreach ($parameters as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        return $this;
    }

    /**
     * Create and initialize a request object
     *
     * @param string $class The request class name.
     * @param array $parameters An associative array of parameters.
     * @return RequestInterface
     */
    protected function createRequest(string $class, array $parameters): RequestInterface
    {
        /** @var AbstractRequest $object */
        $object = new $class($this->httpClient);

        return $object->initialize(array_replace($this->getParameters(), $parameters));
    }

    /**
     * Supports Balance
     *
     * @return boolean True if this gateway supports the balance() method
     */
    public function supportsBalance()
    {
        return method_exists($this, 'balance');
    }

    /**
     * Supports Deliver
     *
     * @return boolean True if this gateway supports the deliver() method
     */
    public function supportsDeliver()
    {
        return method_exists($this, 'deliver');
    }

    /**
     * Supports Message
     *
     * @return boolean True if this gateway supports the message() method
     */
    public function supportsMessage()
    {
        return method_exists($this, 'message');
    }
}
