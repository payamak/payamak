<?php

/**
 * @package Payamak\Common\Exception
 * @subpackage InvalidRequestException
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common\Exception;

use Exception;

/**
 * Invalid Request Exception
 *
 * Thrown when a request is invalid or missing required fields.
 *
 * @package Payamak\Common\Exception
 */
class InvalidRequestException extends Exception implements PayamakException
{
}
