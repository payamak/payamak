<?php

/**
 * @package Payamak\Common\Exception
 * @subpackage InvalidResponseException
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common\Exception;

use Exception;
use Throwable;

/**
 * Invalid Response exception.
 *
 * Thrown when a gateway responded with invalid or unexpected data (for example, a security hash did not match).

 * @package Payamak\Common\Exception
 */
class InvalidResponseException extends Exception implements PayamakException
{
    /**
     * InvalidResponseException constructor.
     *
     * @param string $message The Exception message to throw.
     * @param int $code The Exception code.
     * @param Throwable|null $previous The previous throwable used for the exception chaining.
     */
    public function __construct(string $message = '', int $code = 0, ?Throwable $previous = null)
    {
        $message = $message ?: 'Invalid response from messaging gateway';

        parent::__construct($message, $code, $previous);
    }
}
