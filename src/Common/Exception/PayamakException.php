<?php

/**
 * @package Payamak\Common\Exception
 * @subpackage PayamakException
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common\Exception;

/**
 * Interface PayamakException

 * @package Payamak\Common\Exception
 */
interface PayamakException
{

}
