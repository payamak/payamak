<?php

/**
 * @package Payamak\Common
 * @subpackage GatewayFactory
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common;

use Http\Client\Common\HttpMethodsClient;
use Payamak\Common\Exception\RuntimeException;

/**
 * Class GatewayFactory
 *
 * @package Payamak\Common
 */
class GatewayFactory
{
    /**
     * Create a new gateway instance
     *
     * @param string $class Gateway name.
     * @param HttpMethodsClient|null $httpClient A HTTP Client implementation.
     * @return GatewayInterface
     * @throws RuntimeException If no such gateway is found.
     */
    public function create(string $class, ?HttpMethodsClient $httpClient = null): GatewayInterface
    {
        $class = $this->getGatewayClassName($class);

        if (false === class_exists($class)) {
            throw new RuntimeException('Class \'' . $class . '\' not found');
        }

        return new $class($httpClient);
    }

    /**
     * Resolve a short gateway name to a full namespaced gateway class.
     *
     * @param string $name The short gateway name.
     * @return string
     */
    private function getGatewayClassName(string $name): string
    {
        if (0 === strpos($name, '\\')) {
            return $name;
        }

        return '\\Payamak\\' . $name . '\\Gateway';
    }
}
