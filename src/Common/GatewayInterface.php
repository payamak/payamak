<?php

/**
 * @package Payamak\Common
 * @subpackage GatewayInterface
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common;

use Payamak\Common\Message\RequestInterface;

/**
 * Messaging gateway interface
 *
 * This interface class defines the standard functions that any
 * Payamak gateway needs to define.
 *
 * @package Payamak\Common
 *
 * @method RequestInterface balance(array $parameters = []) Check balance on gateway.
 * @method RequestInterface message(array $parameters = []) Send message using gateway.
 * @method RequestInterface deliver(array $parameters = []) Track delivery status of message.
 * */
interface GatewayInterface
{
    /**
     * Get gateway display name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get gateway short name
     *
     * This name can be used with GatewayFactory as an alias of the gateway class,
     * to create new instances of this gateway.
     *
     * @return string
     */
    public function getShortName(): string;

    /**
     * Define gateway parameters, in the following format:
     *
     * <pre>
     * array(
     *     'username' => '', // string variable
     *     'testMode' => false, // boolean variable
     * );
     * </pre>
     *
     * @return array
     */
    public function getDefaultParameters(): array;

    /**
     * Initialize gateway with parameters
     *
     * @param array $parameters An associative array of parameters.
     * @return $this
     */
    public function initialize(array $parameters = []);

    /**
     * Get all gateway parameters
     *
     * @return array
     */
    public function getParameters(): array;
}
