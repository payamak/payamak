<?php

/**
 * @package Payamak\Common\Http\Exception
 * @subpackage RequestException
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common\Http\Exception;

use Payamak\Common\Exception\PayamakException;

/**
 * Class RequestException
 *
 * @package Payamak\Common\Http\Exception
 */
class RequestException extends \Http\Client\Exception\RequestException implements PayamakException
{

}
