<?php

/**
 * @package Payamak\Common\Message
 * @subpackage AbstractRequest
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common\Message;

use Http\Client\Common\HttpMethodsClient;
use Payamak\Common\Exception\RuntimeException;
use Payamak\Common\ParametersTrait;

/**
 * Class AbstractRequest
 *
 * @package Payamak\Common\Message
 */
abstract class AbstractRequest implements RequestInterface
{
    use ParametersTrait {
        setParameter as traitSetParameter;
    }

    /**
     * A HTTP client
     *
     * @var HttpMethodsClient|null
     */
    protected $httpClient;

    /**
     * An associated ResponseInterface.
     *
     * @var ResponseInterface
     */
    protected $response;

    /**
     * Create a new Request
     *
     * @param HttpMethodsClient|null $httpClient A HTTP client to make API calls with.
     */
    public function __construct(?HttpMethodsClient $httpClient = null)
    {
        $this->httpClient = $httpClient;

        $this->initialize();
    }

    /**
     * Initialize the object with parameters.
     *
     * If any unknown parameters passed, they will be ignored.
     *
     * @param array $parameters An associative array of parameters.
     *
     * @return $this
     * @throws RuntimeException If request modified after sent.
     */
    public function initialize(array $parameters = [])
    {
        if (null !== $this->response) {
            throw new RuntimeException('Request cannot be modified after it has been sent!');
        }

        $this->parameters = [];

        foreach ($parameters as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        return $this;
    }

    /**
     * Set a single parameter
     *
     * @param string $key The parameter key.
     * @param mixed $value The value to set.
     * @return $this
     * @throws RuntimeException If a request parameter is modified after the request has been sent.
     */
    protected function setParameter(string $key, $value)
    {
        if (null !== $this->response) {
            throw new RuntimeException('Request cannot be modified after it has been sent!');
        }

        return $this->traitSetParameter($key, $value);
    }

    /**
     * @inheritDoc
     * @return ResponseInterface
     * @throws RuntimeException If try to get response before sending the request.
     */
    public function getResponse(): ResponseInterface
    {
        if (null === $this->response) {
            throw new RuntimeException('You must call send() before accessing the Response!');
        }

        return $this->response;
    }

    /**
     * @inheritDoc
     * @return ResponseInterface
     */
    public function send(): ResponseInterface
    {
        $data = $this->getData();

        return $this->sendData($data);
    }
}
