<?php

/**
 * @package Payamak\Common\Message
 * @subpackage AbstractResponse
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common\Message;

/**
 * Class AbstractRequest

 * @package Payamak\Common\Message
 */
abstract class AbstractResponse implements ResponseInterface
{
    /**
     * The embodied request object.
     *
     * @var RequestInterface
     */
    protected $request;

    /**
     * The data contained in the response.
     *
     * @var mixed
     */
    protected $data;

    /**
     * Constructor
     *
     * @param RequestInterface $request The initiating request.
     * @param mixed $data Content of request body.
     */
    public function __construct(RequestInterface $request, $data)
    {
        $this->request = $request;
        $this->data = $data;
    }

    /**
     * @inheritDoc
     * @return RequestInterface
     */
    public function getRequest(): RequestInterface
    {
        return $this->request;
    }

    /**
     * Get the response data.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @inheritDoc
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return null;
    }

    /**
     * @inheritDoc
     * @return string|null
     */
    public function getCode(): ?string
    {
        return null;
    }
}
