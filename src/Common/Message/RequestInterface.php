<?php

/**
 * @package Payamak\Common\Message
 * @subpackage RequestInterface
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common\Message;

/**
 * Interface RequestInterface

 * @package Payamak\Common\Message
 */
interface RequestInterface
{
    /**
     * Initialize request with parameters
     *
     * @param array $parameters The parameters to send.
     * @return $this
     */
    public function initialize(array $parameters = []);

    /**
     * Get the response to this request (if the request has been sent)
     *
     * @return ResponseInterface
     */
    public function getResponse();

    /**
     * Get the raw data array for this message. The format of this varies from gateway to
     * gateway, but will usually be either an associative array, or a SimpleXMLElement.
     *
     * @return mixed
     */
    public function getData();

    /**
     * Send the request
     *
     * @return ResponseInterface
     */
    public function send();

    /**
     * Send the request with specified data
     *
     * @param mixed $data The data to send.
     * @return ResponseInterface
     */
    public function sendData($data);
}
