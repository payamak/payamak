<?php

/**
 * @package Payamak\Common\Message
 * @subpackage ResponseInterface
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common\Message;

/**
 * Interface ResponseInterface
 *
 * @package Payamak\Common\Message
 */
interface ResponseInterface
{
    /**
     * Get the original request which generated this response
     *
     * @return RequestInterface
     */
    public function getRequest(): RequestInterface;

    /**
     * Is the response successful?
     *
     * @return boolean
     */
    public function isSuccessful(): bool;

    /**
     * Response Message
     *
     * @return null|string A response message from the payment gateway
     */
    public function getMessage(): ?string;

    /**
     * Response code
     *
     * @return null|string A response code from the payment gateway
     */
    public function getCode(): ?string;
}
