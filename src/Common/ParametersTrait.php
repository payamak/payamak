<?php

/**
 * @package Payamak\Common
 * @subpackage ParametersTrait
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak\Common;

use Payamak\Common\Exception\InvalidRequestException;

/**
 * Trait ParametersTrait

 * @package Payamak\Common
 */
trait ParametersTrait
{
    /**
     * @var array
     */
    protected $parameters;

    /**
     * Set one parameter.
     *
     * @param string $key Parameter key.
     * @param mixed $value Parameter value.
     * @return $this
     */
    protected function setParameter(string $key, $value): self
    {
        $this->parameters[$key] = $value;

        return $this;
    }

    /**
     * Get one parameter.
     *
     * @param string $key Parameter key.
     * @return mixed
     */
    protected function getParameter(string $key)
    {
        return array_key_exists($key, $this->parameters) ? $this->parameters[$key] : null;
    }

    /**
     * Get all parameters.
     *
     * @return array An associative array of parameters.
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * Validate the request.
     *
     * This method is called internally by gateways to avoid wasting time with an API call
     * when the request is clearly invalid.
     *
     * @param mixed ...$args A variable length list of required parameters.
     * @return void
     * @throws InvalidRequestException If required parameter is missing.
     */
    public function validate(...$args): void
    {
        foreach ($args as $key) {
            $value = $this->getParameter($key);
            if (false === isset($value)) {
                throw new InvalidRequestException('The ' . $key . ' parameter is required');
            }
        }
    }

    /**
     * @return string|null
     */
    public function getTo(): ?string
    {
        return $this->getParameter('to');
    }

    /**
     * @param string $value Value of the to parameter.
     * @return $this
     */
    public function setTo(string $value)
    {
        $this->setParameter('to', $value);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFrom(): ?string
    {
        return $this->getParameter('from');
    }

    /**
     * @param string $value Value of the from parameter.
     * @return $this
     */
    public function setFrom(string $value)
    {
        $this->setParameter('from', $value);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->getParameter('text');
    }

    /**
     * @param string $value Value of the text parameter.
     * @return $this
     */
    public function setText(string $value)
    {
        $this->setParameter('text', $value);

        return $this;
    }

    /**
     * @return boolean
     */
    public function getTestMode(): bool
    {
        return $this->getParameter('testMode');
    }

    /**
     * @param boolean $value Value of the testMode parameter.
     * @return $this
     */
    public function setTestMode(bool $value)
    {
        $this->setParameter('testMode', $value);

        return $this;
    }
}
