<?php

/**
 * @package Payamak
 * @subpackage Payamak
 * @author Milad Nekofar <milad@nekofar.com>
 * @copyright 2019-2020 Milad Nekofar
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

declare(strict_types=1);

namespace Payamak;

use Payamak\Common\GatewayFactory;

/**
 * Class Payamak

 * @package Payamak
 */
class Payamak
{
    /**
     * Internal factory storage
     *
     * @var GatewayFactory
     */
    private static $factory;

    /**
     * Static function call router.
     *
     * All other function calls to the Payamak class are routed to the
     * factory.
     *
     * Example:
     *
     * <code>
     *   // Create a gateway for the Twilio
     *   $gateway = Payamak::create('Twilio');
     * </code>
     *
     * @param string $method The factory method to invoke.
     * @param array $parameters Parameters passed to the factory method.
     *
     * @return mixed
     * @see GatewayFactory
     */
    public static function __callStatic(string $method, array $parameters)
    {
        $factory = self::getFactory();

        return call_user_func_array([$factory, $method], $parameters);
    }

    /**
     * Get the gateway factory
     *
     * @return GatewayFactory
     */
    public static function getFactory(): GatewayFactory
    {
        if (null === self::$factory) {
            self::$factory = new GatewayFactory();
        }

        return self::$factory;
    }

    /**
     * Set the gateway factory
     *
     * @param GatewayFactory $factory A GatewayFactory instance.
     * @return void
     */
    public static function setFactory(?GatewayFactory $factory): void
    {
        self::$factory = $factory;
    }
}
