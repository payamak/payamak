<?php

/**
 * @package Payamak\Tests\Common
 */

namespace Payamak\Tests\Common;

use Payamak\Common\AbstractGateway;
use Payamak\Common\GatewayInterface;
use Payamak\Common\Message\AbstractRequest;
use Payamak\Common\Message\RequestInterface;
use Payamak\Tests\TestCase;

/**
 * Class AbstractGatewayTest
 */
class AbstractGatewayTest extends TestCase
{
    /**
     * @var GatewayInterface
     */
    private $gateway;

    /**
     *
     */
    protected function setUp(): void
    {
        $this->gateway = $this->getMockForAbstractClass(
            AbstractGateway::class,
            [$this->getMethodsClient($this->getMockClient())]
        );
        $this->gateway->initialize();

        if (!class_exists('\Payamak\Twilio\Gateway')) {
            class_alias(get_class($this->gateway), '\Payamak\Twilio\Gateway');
        }
    }

    /**
     * @test
     */
    public function testConstruct()
    {
        $this->gateway = $this->getMockForAbstractClass(
            AbstractGateway::class, [null]
        );

        $this->assertInstanceOf(AbstractGateway::class, $this->gateway);
        $this->assertSame([], $this->gateway->getParameters());
    }

    /**
     * @test
     */
    public function testGetShortName()
    {
        $this->assertSame('\\' . get_class($this->gateway), $this->gateway->getShortName());
    }

    /**
     * @test
     */
    public function testGetDefaultParameters()
    {
        $this->assertSame([], $this->gateway->getDefaultParameters());
    }

    /**
     * @test
     */
    public function testInitializeDefaults()
    {
        $this->gateway = $this->getMockForAbstractClass(
            AbstractGateway::class,
            [$this->getMethodsClient($this->getMockClient())],
            '',
            false,
            false,
            true,
            ['getDefaultParameters']
        );

        $this->gateway->method('getDefaultParameters')
            ->willReturn(['from' => '123456']);

        $this->gateway->initialize();

        $this->assertSame(['from' => '123456'], $this->gateway->getParameters());
    }

    /**
     * @test
     */
    public function testInitializeParameters()
    {
        $this->gateway = $this->getMockForAbstractClass(
            AbstractGateway::class,
            [$this->getMethodsClient($this->getMockClient())],
            '',
            false,
            false,
            true,
            ['getDefaultParameters']
        );

        $this->gateway->initialize(['from' => '123456']);

        $this->assertSame(['from' => '123456'], $this->gateway->getParameters());
    }

    /**
     * @test
     */
    public function testSupportsBalance()
    {
        $this->assertFalse($this->gateway->supportsBalance());
    }

    /**
     * @test
     */
    public function testSupportsDeliver()
    {
        $this->assertFalse($this->gateway->supportsDeliver());
    }

    /**
     * @test
     */
    public function testSupportsMessage()
    {
        $this->assertFalse($this->gateway->supportsMessage());
    }

    /**
     * @test
     */
    public function testCreateRequest()
    {

        $gateway = $this->getMockForAbstractClass(
            AbstractGateway::class,
            [$this->getMethodsClient($this->getMockClient())],
            '',
            true,
            false,
            true,
            ['message']
        );

        $request = $this->getMockForAbstractClass(
            AbstractRequest::class
        );

        $gateway->expects($this->any())
            ->method('message')
            ->willReturn($request);


        $caller = function () use ($request) {
            return $this->createRequest(get_class($request), []);
        };

        $bound = $caller->bindTo($gateway, $gateway);

        $this->assertInstanceOf(RequestInterface::class, $bound());
    }
}
