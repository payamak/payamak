<?php

/**
 * @package Payamak\Tests\Common\Exception
 */

namespace Payamak\Tests\Common\Exception;

use Payamak\Common\Exception\InvalidResponseException;
use Payamak\Tests\TestCase;

/**
 * Class InvalidResponseExceptionTest
 */
class InvalidResponseExceptionTest extends TestCase
{
    /**
     * @test
     */
    public function testConstructWithDefaultMessage()
    {
        $exception = new InvalidResponseException();
        $this->assertSame('Invalid response from messaging gateway', $exception->getMessage());
    }

    /**
     * @test
     */
    public function testConstructWithCustomMessage()
    {
        $exception = new InvalidResponseException('Oops');
        $this->assertSame('Oops', $exception->getMessage());
    }
}
