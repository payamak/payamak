<?php

/**
 * @package Payamak\Tests\Common\Exception
 */

namespace Payamak\Tests\Common\Exception;

use Payamak\Common\Exception\RuntimeException;
use Payamak\Tests\TestCase;

/**
 * Class RuntimeExceptionTest
 */
class RuntimeExceptionTest extends TestCase
{
    /**
     * @test
     */
    public function testConstruct()
    {
        $exception = new RuntimeException('Oops');
        $this->assertSame('Oops', $exception->getMessage());
    }
}
