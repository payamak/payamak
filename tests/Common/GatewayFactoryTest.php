<?php

/**
 * @package Payamak\Tests\Common
 */

namespace Payamak\Tests\Common;

use Payamak\Common\AbstractGateway;
use Payamak\Common\Exception\RuntimeException;
use Payamak\Common\GatewayFactory;
use Payamak\Tests\TestCase;

/**
 * Class GatewayFactoryTest
 */
class GatewayFactoryTest extends TestCase
{

    /**
     * @var GatewayFactory
     */
    private $factory;

    /**
     * @var AbstractGateway
     */
    private $gateway;

    /**
     *
     */
    protected function setUp(): void
    {
        $this->gateway = $this->getMockForAbstractClass(
            AbstractGateway::class,
            [$this->getMethodsClient($this->getMockClient())]
        );
        $this->gateway->initialize();

        if (!class_exists('\Payamak\Twilio\Gateway')) {
            class_alias(get_class($this->gateway), '\Payamak\Twilio\Gateway');
        }

        $this->factory = new GatewayFactory();
    }

    /**
     * @test
     */
    public function testCreateInvalid()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage("Class '\Payamak\Invalid\Gateway' not found");

        $this->gateway = $this->factory->create('Invalid');
    }

    /**
     * @test
     */
    public function testCreateShortName(): void
    {
        $this->gateway = $this->factory->create('Twilio');

        $this->assertInstanceOf(AbstractGateway::class, $this->gateway);
        $this->assertInstanceOf('\\Payamak\\Twilio\\Gateway', $this->gateway);
    }

    /**
     * @test
     */
    public function testCreateFullyQualified(): void
    {
        $this->gateway = $this->factory->create('\\Payamak\\Twilio\\Gateway');

        $this->assertInstanceOf(AbstractGateway::class, $this->gateway);
        $this->assertInstanceOf('\\Payamak\\Twilio\\Gateway', $this->gateway);
    }
}
