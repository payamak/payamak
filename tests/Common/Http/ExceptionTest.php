<?php

/**
 * @package Payamak\Tests\Common\Http
 */

namespace Payamak\Tests\Common\Http;

use GuzzleHttp\Psr7\Request;
use Payamak\Common\Http\Exception\NetworkException;
use Payamak\Tests\TestCase;

/**
 * Class ExceptionTest
 */
class ExceptionTest extends TestCase
{
    public function testConstruct()
    {
        $request = new Request('GET', '/path');

        $previous = new \Exception('Whoops');
        $exception = new NetworkException('Something went wrong', $request, $previous);

        $this->assertSame($request, $exception->getRequest());
        $this->assertSame('Something went wrong', $exception->getMessage());
        $this->assertSame($previous, $exception->getPrevious());
    }
}