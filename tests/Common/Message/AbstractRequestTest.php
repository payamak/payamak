<?php

namespace Payamak\Tests\Common\Message;

use Payamak\Common\Exception\InvalidRequestException;
use Payamak\Common\Exception\RuntimeException;
use Payamak\Common\Message\AbstractRequest;
use Payamak\Common\Message\AbstractResponse;
use Payamak\Common\Message\ResponseInterface;
use Payamak\Tests\TestCase;
use ReflectionException;

class AbstractRequestTest extends TestCase
{
    /**
     * @var AbstractRequest
     */
    protected $request;

    /**
     *
     */
    protected function setUp(): void
    {
        $this->request = $this->getMockForAbstractClass(
            AbstractRequest::class,
            [$this->getMethodsClient($this->getMockClient())]
        );

        $this->request->initialize();
    }

    /**
     * @test
     */
    public function testInitializeWithParams()
    {
        $this->assertSame($this->request, $this->request->initialize(['text' => 'Some text.']));
        $this->assertSame('Some text.', $this->request->getText());
    }

    /**
     * @throws ReflectionException
     */
    public function testInitializeAfterRequestSent()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Request cannot be modified after it has been sent!');

        $response = $this->getMockForAbstractClass(
            AbstractResponse::class,
            [$this->request, []]
        );

        $this->setProtectedProperty(
            $this->request,
            'response',
            $response
        );

        $this->request->expects($this->any())
            ->method('sendData')
            ->willReturn($response);

        $this->request->send();

        $this->request->initialize();
    }

    /**
     * @test
     */
    public function testTo()
    {
        $this->assertSame($this->request, $this->request->setTo('12345'));
        $this->assertSame('12345', $this->request->getTo());
    }

    /**
     * @test
     */
    public function testFrom()
    {
        $this->assertSame($this->request, $this->request->setFrom('12345'));
        $this->assertSame('12345', $this->request->getFrom());
    }

    /**
     * @test
     */
    public function testBody()
    {
        $this->assertSame($this->request, $this->request->setText('Some text.'));
        $this->assertSame('Some text.', $this->request->getText());
    }

    /**
     * @test
     */
    public function testInitializedParametersAreSet()
    {
        $params = ['testMode' => false];

        $this->request->initialize($params);

        $this->assertSame($this->request->getTestMode(), false);
    }

    /**
     * @test
     */
    public function testGetParameters()
    {
        $this->request->setTestMode(true);
        $this->request->setFrom('123456');

        $expected = array(
            'testMode' => true,
            'from' => '123456',
        );
        $this->assertEquals($expected, $this->request->getParameters());
    }

    /**
     * @throws ReflectionException
     */
    public function testSetParameterAfterRequestSent()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Request cannot be modified after it has been sent!');

        $response = $this->getMockForAbstractClass(
            AbstractResponse::class,
            [$this->request, []]
        );

        $this->setProtectedProperty(
            $this->request,
            'response',
            $response
        );

        $this->request->expects($this->any())
            ->method('sendData')
            ->willReturn($response);

        $this->request->send();

        $this->request->setText('Some text.');
    }

    /**
     * @throws InvalidRequestException
     */
    public function testCanValidateExistingParameters()
    {
        $this->request->setTestMode(true);
        $this->request->setFrom('123456');

        $this->assertNull($this->request->validate('testMode', 'from'));
    }

    /**
     * @throws InvalidRequestException
     */
    public function testInvalidParametersThrowsException()
    {
        $this->expectException(InvalidRequestException::class);

        $this->request->setTestMode(true);

        $this->request->validate('testMode', 'from');
    }

    /**
     * @test
     */
    public function testSend()
    {
        $response = $this->createMock(ResponseInterface::class);
        $data = array('request data');

        $this->request->method('getData')->willReturn($data);
        $this->request->method('sendData')->willReturn($response);

        $this->assertSame($response, $this->request->send());
    }

    /**
     * @test
     */
    public function testGetResponseBeforeRequestSent()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('You must call send() before accessing the Response!');

        $this->request->getResponse();
    }

    /**
     * @throws ReflectionException
     */
    public function testGetResponseAfterRequestSent()
    {
        $response = $this->getMockForAbstractClass(
            AbstractResponse::class,
            [$this->request, []]
        );

        $this->setProtectedProperty(
            $this->request,
            'response',
            $response
        );

        $this->request->expects($this->any())
            ->method('sendData')
            ->willReturn($response);

        $this->request->send();

        $response = $this->request->getResponse();
        $this->assertInstanceOf(ResponseInterface::class, $response);
    }
}
