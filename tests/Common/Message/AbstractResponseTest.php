<?php

/**
 *
 */

namespace Payamak\Tests\Common\Message;

use Payamak\Common\Message\AbstractResponse;
use Payamak\Common\Message\RequestInterface;
use Payamak\Tests\TestCase;

/**
 * Class AbstractResponseTest
 * @package Payamak\Tests\Common\Message
 */
class AbstractResponseTest extends TestCase
{
    /**
     * @var AbstractResponse
     */
    protected $response;

    /**
     *
     */
    protected function setUp(): void
    {
        $this->response = $this->getMockForAbstractClass(
            AbstractResponse::class,
            [$this->createMock(RequestInterface::class), null]
        );
    }

    /**
     * @test
     */
    public function testConstruct()
    {
        $data = ['foo' => 'bar'];
        $request = $this->createMock(RequestInterface::class);

        $this->response = $this->getMockForAbstractClass(
            AbstractResponse::class,
            [$request, $data]
        );

        $this->assertSame($request, $this->response->getRequest());
        $this->assertSame($data, $this->response->getData());
    }

    /**
     * @test
     */
    public function testDefaultMethods()
    {
        $this->assertNull($this->response->getData());
        $this->assertNull($this->response->getMessage());
        $this->assertNull($this->response->getCode());
    }

}
