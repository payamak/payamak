<?php

/**
 * @package Payamak\Tests
 */

namespace Payamak\Tests;

use Payamak\Common\GatewayFactory;
use Payamak\Payamak;

/**
 * Class PayamakTest
 */
class PayamakTest extends TestCase
{
    /**
     * @test
     */
    public function testGetFactory()
    {
        Payamak::setFactory(null);

        $factory = Payamak::getFactory();
        $this->assertInstanceOf(GatewayFactory::class, $factory);
    }

    /**
     * @test
     */
    public function testSetFactory()
    {
        /** @var GatewayFactory $factory */
        $factory = $this->getMockBuilder(GatewayFactory::class)->getMock();

        Payamak::setFactory($factory);

        $this->assertInstanceOf(GatewayFactory::class, Payamak::getFactory());
    }

    /**
     * @test
     */
    public function testCallStatic()
    {
        $factory = $this->getMockBuilder(GatewayFactory::class)
            ->addMethods(['testMethod'])
            ->getMock();

        $factory->method('testMethod')
            ->with('some-argument')
            ->willReturn('some-result');

        /** @var GatewayFactory $factory */
        Payamak::setFactory($factory);

        /** @noinspection PhpUndefinedMethodInspection */
        $result = Payamak::testMethod('some-argument');
        $this->assertSame('some-result', $result);
    }

    /**
     *
     */
    protected function tearDown(): void
    {
        Payamak::setFactory(null);

        parent::tearDown();
    }
}
